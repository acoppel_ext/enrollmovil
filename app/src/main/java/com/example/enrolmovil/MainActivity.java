package com.example.enrolmovil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.coppel.josesolano.enrollmovil.MainActivityEnrollMovil;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    static final String ACT_HUELLAS_OPC = "IN_1"; //1=AMBAS - 2=DERECHA - 3=IZQUIERDA - 4=VERIFICACIÓN
    static final String ACT_HUELLAS_NUM = "IN_2";
    static final String ACT_HUELLAS_IMG = "IN_3"; //0=NO - 1=SI
    static final String ACT_HUELLAS_OT1 = "OUT_1"; //CODIGO DE RESPUESTA
    static final String ACT_HUELLAS_OT2 = "OUT_2"; //JSON


    private Spinner bandera, entrada;
    private Button enviar;
    EditText folio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setComponentes();
        setListeners();

        spinnerBanderaVerificacion();
        spinnerEntrada();

    }

    private void spinnerBanderaVerificacion() {

        ArrayList<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact( "1", "" + 1));
        contacts.add(new Contact( "0", "" + 0));

        ArrayAdapter<Contact> adapter =
                new ArrayAdapter<Contact>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, contacts);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        bandera.setAdapter(adapter);

    }

    private void spinnerEntrada() {

        ArrayList<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact( "1", "" + 1));
        contacts.add(new Contact( "2", "" + 2));
        contacts.add(new Contact( "3", "" + 3));
        contacts.add(new Contact( "4", "" + 4));
        contacts.add(new Contact( "5", "" + 5));
        contacts.add(new Contact( "6", "" + 6));
        contacts.add(new Contact( "7", "" + 7));
        contacts.add(new Contact( "8", "" + 8));
        contacts.add(new Contact( "9", "" + 9));

        ArrayAdapter<Contact> adapter =
                new ArrayAdapter<Contact>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, contacts);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        entrada.setAdapter(adapter);

    }

    public void setListeners(){
        /*ON CLICK LISTENER*/
        enviar.setOnClickListener(this);
    }

    public void setComponentes(){
        //COMPONENTES DE CABECERA PARA MOSTRAR NOMBRE
        enviar  = findViewById(R.id.enviar);
        folio  = findViewById(R.id.folio);
        bandera = (Spinner) findViewById(R.id.banderaVerificacion);
        entrada = (Spinner) findViewById(R.id.entrada);
    }

    private void llamaComponenteHuellas(String folio, String banderaVerificacion, String entrada){
        /*LLAMADO COMPONENTE DE HUELLAS*/
        Intent intent = new Intent(MainActivity.this, MainActivityEnrollMovil.class);
        //intent.setComponent(new ComponentName(ACT_HUELLAS_PKG,ACT_HUELLAS_CLS));
        if (intent != null){
            Bundle extra = new Bundle();
            extra.putInt(ACT_HUELLAS_OPC, Integer.parseInt(entrada));
            extra.putInt(ACT_HUELLAS_NUM, Integer.parseInt(folio));
            extra.putInt(ACT_HUELLAS_IMG, Integer.parseInt(banderaVerificacion));
            intent.putExtras(extra);

            //startActivityForResult(intent,100);
            startActivityForResult(intent, 100);
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        Bundle resApp = data.getExtras();
        Integer OUT_1 = resApp.getInt    (ACT_HUELLAS_OT1);
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                String resultado = data.getStringExtra("OUT_1");
                System.out.println("Salida: " +    OUT_1);

            }

    }
    @Override
    public void onClick(View view) {

        if(view.getId() == enviar.getId()) {

/*

            String[] s = {"Seleccione","Cartilla Militar","Cedula Profesional","Credencial de Elector","Documento Migratorio, Pasaporte"};

            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this);
            View mView = getLayoutInflater().inflate(R.layout.dialog_spinner, null);
            builder.setTitle("Tipo de Documento Probatorio:");
            final ArrayAdapter<String> adp = new ArrayAdapter<String>(MainActivity.this,
                    android.R.layout.simple_spinner_item, s);

            final Spinner sp = new Spinner(MainActivity.this);
            sp.setLayoutParams(new LinearLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT));
            sp.setAdapter(adp);
            builder.setCancelable(false);
            builder.setPositiveButton("Aceptar",new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    if(!sp.getSelectedItem().toString().equals("Seleccione")){
                        Toast.makeText(MainActivity.this,
                                sp.getSelectedItem().toString(),Toast.LENGTH_SHORT).show();
                        System.out.println("AQUI___________"+sp.getSelectedItemId());
                        dialog.dismiss();
                    }

                }
            });
            builder.setView(sp);
            builder.setMessage("");
            android.app.AlertDialog dialog = builder.create();
            dialog.show();
* */


            System.out.println("Folio: " +  folio.getText().toString());
            System.out.println("bandera: " +    bandera.getSelectedItem().toString());
            System.out.println("Entrada: " +    entrada.getSelectedItem().toString());

            llamaComponenteHuellas(folio.getText().toString(),bandera.getSelectedItem().toString(),entrada.getSelectedItem().toString());
        }

    }


    private class Contact {
        private String contact_name;
        private String contact_id;

        public Contact() {
        }

        public Contact(String contact_name, String contact_id) {
            this.contact_name = contact_name;
            this.contact_id = contact_id;
        }

        public String getContact_name() {
            return contact_name;
        }

        public void setContact_name(String contact_name) {
            this.contact_name = contact_name;
        }

        public String getContact_id() {
            return contact_id;
        }

        public void setContact_id(String contact_id) {
            this.contact_id = contact_id;
        }

        /**
         * Pay attention here, you have to override the toString method as the
         * ArrayAdapter will reads the toString of the given object for the name
         *
         * @return contact_name
         */
        @Override
        public String toString() {
            return contact_name;
        }
    }
}
