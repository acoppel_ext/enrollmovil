package com.example.enrolmovil;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class Utilerias {

    static final String apikey                 = "6580185501b0c428915cc04abee861af";
    static final int SOAP_OBTENER_IP            = 170;
    static final int SOAP_COMPARACION_TEMPLATE  = 150; //Nueva Comparacion
    static final String REST_CLIE_WIFI = "https://biometrics.coppel.io/afocustomer";
    static final int SOAP_GUARDAR_EMP_AUTORIZA  = 160;
    //ARCHIVO CONFIGURACIÓN
    static final String CONFIG_KEY1 = "encoioui";

    //NOMBRES APLICACIONES NECESARIAS
    static final String NAME_LECTOR_HUELLAS = "EnrollMovil";
    static final String NAME_FIRMA_ENROLA   = "Afil. Firma Digit.";
    static final String NAME_DIGITALIZADOR  = "Afil. Digitalizador";
    static final String NAME_REENROLAMIENTO = "Reenrolamiento Móvil";
    static final String NAME_MODULO         = "Menú Módulo Afore";
    static final String NAME_AFILIACION = "Afiliación Trabajador";
    static final String NAME_VIDEO = "Grabador Video";

    //LLAMAR_DIGITALIZADOR
    static final String ACT_DIGI_PKG = "com.aforecoppelafil.my.digitalizadorafilmovil";
    static final String ACT_DIGI_CLS = "com.aforecoppelafil.my.digitalizadorafilmovil.MainListActivity";
    static final String ACT_DIGI_TIPO   = "tipoOperacion";  //int
    static final String ACT_DIGI_TABLA  = "tablaConsulta";  //int
    static final String ACT_DIGI_FOAFI  = "folioAfiliacion";//long
    static final String ACT_DIGI_FOSER  = "folioServicio";  //long
    static final String ACT_DIGI_NOEMP  = "numEmp";         //long

    // SERVIDORES
    static final String IP_SERV_MOVIL  = "http://10.27.142.196:50131";
    static final String IP_SERV_DEV  = "http://10.44.172.234";
    static final String IP_SERV_QA_WS = "http://10.27.142.197:50131";
    static final String IP_SERV_QA = "http://10.27.142.242";
    static final String IP_SERV_PROD_WS = "http://10.26.190.158:50131";
    static final String IP_SERV_PROD = "http://10.26.190.175";


    static final String ACT_AFILIACION_NUM = "empleado";
    static final String ACT_AFILIACION_NOM = "nombre";
    static final String ACT_AFILIACION_CEN = "centro";

    static final int  LLAMAR_DIGITALIZADOR       = 305;
    static final int  LLAMAR_DIALOGO_EXCEP       = 302;
    static final int  LLAMAR_FIRMA_ENROLA        = 304;
    static final int  LLAMAR_FORMATO_ENROL       = 303;
    static final int  LLAMAR_LECTOR_HUELLAS      = 100;
    static final int  LLAMAR_ENROL_FOLIO      = 310;
    static final int  LLAMAR_LECTOR_HUELLASV      = 311;
    static final int  LLAMAR_WEB_TIPO        = 312;
    static final int  LLAMAR_WEB_AFILIACION        = 313;
    static final int  LLAMAR_WEB_IMPRESION        = 314;
    static final int  LLAMAR_WEB_CAPTURA        = 315;
    static final int  LLAMAR_GRABAR_VIDEO        = 316;
    static final int  LLAMAR_WEB_ENLACE        = 317;
    static final int  LLAMAR_WEB_DOCTORN        = 318;
    static final int  LLAMAR_WEB_ENLACE_INE        = 319;

    //---LECTOR_HUELLAS_ACTIVITY
    static final String ACT_HUELLAS_PKG = "com.example.leonardofigueroa.enrolmovil";
    static final String ACT_HUELLAS_CLS = "com.example.leonardofigueroa.enrolmovil.MainActivity";
    static final String ACT_HUELLAS_OPC = "IN_1"; //1=AMBAS - 2=DERECHA - 3=IZQUIERDA - 4=VERIFICACIÓN
    static final String ACT_HUELLAS_NUM = "IN_2";
    static final String ACT_HUELLAS_IMG = "IN_3"; //0=NO - 1=SI
    static final String ACT_HUELLAS_OT1 = "OUT_1"; //CODIGO DE RESPUESTA
    static final String ACT_HUELLAS_OT2 = "OUT_2"; //JSON
    public static final String TAG_RESP_FOLIO    = "Folio";
    static final String ACT_TIPO_AFI    = "afiliacion";//String
    static final String PARAM_OPCION_TRAMITE    = "OPCION_TRAMITE";//String


    static final String ACT_FORMATO_PKG = "com.aforecoppel.afiliaciontrabajador";
    static final String ACT_FORMATO_CLS = "com.aforecoppel.afiliaciontrabajador.FormatoEnrolActivity";

    static final String ACT_ENROL_PKG = "com.aforecoppel.afiliaciontrabajador";
    static final String ACT_ENROL_CLS = "com.aforecoppel.afiliaciontrabajador.WebEnrolamiento";

    static final String ACT_WEBVIEW_PKG = "com.aforecoppel.afiliaciontrabajador";
    static final String ACT_WEBVIEW_CLS = "com.aforecoppel.afiliaciontrabajador.WebVisorAfiliacion";

    //---LLAMAR FIRMA DIGITAL
    static final String ACT_FIRM_PKG = "com.aforecoppel.my.firmadigitalafiliacion";
    static final String ACT_FIRM_CLS = "com.aforecoppel.my.firmadigitalafiliacion.SignActivity";
    static final String ACT_FIRMA_FOAFI = "sFolioAfiliacion";
    static final String ACT_FIRMA_FOSER = "sFolioServicio";
    static final String ACT_FIRMA_TIPOF = "tipoFirma";
    static final String ACT_FIRMA_NOMFIR= "nombreFirma";
    static final String ACT_FIRMA_PERSONAF = "personaFirma";

    static final String ACT_NOMBRE_LLAMADA= "nombreActivity";

    //---LLAMAR GRABAR VIDEO
    static final String ACT_VIDEO_PKG = "com.android.video";
    static final String ACT_VIDEO_CLS = "com.android.video.CamaraActivity";

    //RUTAS CARPETAS
    static final String DIR_EXTERNA = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
    static final String DIR_DIGITAL = "Digitalizador/";
    static final String DIR_LOGS    = "Log/";
    static final String DIR_TEMP    = "Temp/";
    static final String DIR_HUELLAS = "Huellas/";
    static final String DIR_FORMATO = "FormatoEnrolamiento/";

    //ARCHIVO CONFIGURACIÓN
    static final String CONFIG_NAME = ".config.json";
    static final String CONFIG_KEY2 = "ó~t5nbyr#G_%h4X+(ed`^sa*#&42*cü°";
    static final String CONFIG_KEY3 = "R%#M]OI%B5tr34tv";
    static final String CONFIG_KEY4 = "|OI78mi675v32c1)";

    //HOST Y METODOS APIREST
    static final String REST_EMPL = "https://biometrics.coppel.io/afoemployee";//"https://10.27.113.124:2013";//"https://bioafoemployee.coppel.io";
    static final String REST_CLIE = "https://biometrics.coppel.io/afocustomer";//"https://10.27.113.124:2014";//"https://bioafocustomer.coppel.io";
    static final String REST_IMGE = "/bio/imgenroll";  //CREAR TEMPLATES
    static final String REST_TENF = "/bio/tenfingers"; //COMPARAR TEMPLATES

    static final String DIR_REENROL = "Afiliacion/";
    static final String DIR_AFILIACION = "Afiliacion/";
    static final String LOG_AFILIACION = DIR_EXTERNA + DIR_AFILIACION + DIR_LOGS + "logAfiliacionMovil";

    //MANEJO DE HUELLAS
    static final int HUELLAS_IZQ_FALTA      = 2;
    static final int HUELLAS_DER_FALTA      = 3;

    //PAQUETES APKS
    static final String PAQREE = "com.aforecoppel.my.reenrolamientomovil";
    static final String PAQDIG = "com.aforecoppelafil.my.digitalizadorafilmovil";
    static final String PAQMOD = "com.aforecoppel.my.modulo";
    static final String PAQFIR = "com.aforecoppel.my.firmadigitalafiliacion";
    static final String PAQAFI = "com.aforecoppel.afiliaciontrabajador";
    static final String PAQVID = "com.android.video";

    //RESPUESTAS APIREST
    static final String REST_RP_EST = "Status";
    static final String REST_RP_MEN = "Message";
    static final String REST_RP_RES = "Resultado";
    static final String REST_RP_FRT = "FingerTemplate";

    //IDENTIFICADORES PARA LA LLAMADA AL SERVICIO wsafiliacionmovil
    static final int SOAP_FOLIO_ENROLAMIENTO    = 124;
    static final int SOAP_GUARDAR_HUELLAS       = 125;
    static final int SOAP_GEN_FORMATO_REENROL   = 126;
    static final int SOAP_MOVER_IMG_FIRMA       = 128;
    static final int SOAP_CONVERTIR_TEMPLATE_DER   = 129;
    static final int SOAP_CONVERTIR_TEMPLATE_IZQ   = 130;

    //---MOSTRAR_FORMATO_ENROL
    static final String ACT_FORMATO_PDF   = "namePDF";
    static final String ACT_FORMATO_FOAFI = "sFolioAfiliacion";
    static final String ACT_FORMATO_FOSER = "sFolioServicio";
    static final String ACT_FORMATO_FIRMA = "tipoFirma";

    /**
     *VALORES ESTÁTICOS DE ETIQUETAS DEL WEBSERVICE
     ***/
    private static final String WS_TAG_BASE         = "outParam";
    private static final String WS_TAG_ESTADO       = "EstadoProc";
    public static final String WS_TAG_MSG           = "Mensaje";
    public static final String WS_TAG_HTTP_ESTADO   = "Estatus";
    private static final String TAG_OBT_DOCTOS   = "DoctosDigi";
    private static final String TAG_DEFAULT_LBL  = "EMPTY";
    public static final String TAG_RESP_TIPO_SOL = "TipoSolicitud";
    static final String TAG_RESP_INT_DOC    = "iDocPos";
    static final String TAG_RESP_DOC    = "iPosDoc";

    static final int PERMISSION_ALL = 50;
    static final String[] PERMISSIONS = {
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.READ_EXTERNAL_STORAGE
    };

    static HashMap<String, String> getValuesXML(String xmlMen){

        HashMap<String, String> values = new HashMap<>();

        try {
            DocumentBuilder newDocumentBuilder =
                    DocumentBuilderFactory.newInstance().newDocumentBuilder();

            Document parse = newDocumentBuilder.parse(
                    new ByteArrayInputStream(xmlMen.getBytes()));

            //LA ETIQUETA OutParam CONTIENE LA INFORMACIÓN QUE REGRESA EL SERVICIO
            NodeList node = parse.getElementsByTagName("outParam");
            Node elem = node.item(0);

            Node child;
            String name;

            if(elem != null){
                if(elem.hasChildNodes()){
                    for( child = elem.getFirstChild(); child != null; child = child.getNextSibling() ){
                        name = child.getNodeName();
                        switch (name) {
                            case "EstadoProc": {
                                Node n = parse.getElementsByTagName("EstadoProc").item(0);
                                values.put("Estatus", n.getFirstChild().getTextContent());
                                values.put("EsMensaje", n.getFirstChild().getNextSibling().getTextContent());
                                break;
                            }
                            case "TemplatesProm": {
                                Node n = parse.getElementsByTagName("TemplatesProm").item(0);

                                Node hijos;
                                for (hijos = n.getFirstChild(); hijos != null; hijos = hijos.getNextSibling()) {

                                    String finger = hijos.getFirstChild().getNodeName();
                                    String dedo = hijos.getFirstChild().getTextContent();

                                    String template = hijos.getFirstChild().getNextSibling().getTextContent();

                                    values.put(finger + dedo, template);
                                }
                                break;
                            }
                            default:
                                values.put(child.getNodeName(), child.getTextContent());
                                break;
                        }
                    }
                }
            }

            return values;

        }catch(IOException e){
            e.getStackTrace();
            return null;
        } catch(ParserConfigurationException e){
            e.getStackTrace();
            return null;
        } catch(SAXException e){
            e.getStackTrace();
            return null;
        }
    }

    static void showToast(Activity act, String men){
        Toast.makeText(act, men, Toast.LENGTH_LONG).show();
    }

    static String getTokenAccess(String apikey, boolean isTokenAccess){
        String hashHex = "";
        String cadena = "";
        String mac = getMacAddr();
        String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date()); //'2018-10-29'

        if(isTokenAccess) {
            if (!mac.equals(""))
                cadena = apikey + todayDate + mac;
            else
                writeLog("Ocurrió un problema al generar el TokenAccess");
        }
        else
            cadena = apikey + todayDate;

        hashHex = bin2hex(getHash(cadena));

        return hashHex;
    }
    static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    String hex = Integer.toHexString(b & 0xFF);
                    if (hex.length() == 1)
                        hex = "0".concat(hex);
                    res1.append(hex).append(":");
                }

                res1.deleteCharAt(res1.length()-1);

                return res1.toString();
            }
        } catch (Exception ex) {
            //writeLog("Error en getMacAddr" + ex.getMessage());
            ex.printStackTrace();
        }
        return "";
    }

    static void writeLog(String strLog){

        String todayTime = new SimpleDateFormat("'|'dd-MM-yy'|'HH:mm:ss'|'").format(new Date());
        String todayLog = new SimpleDateFormat("dd-MM-yy").format(new Date());

        strLog = todayTime + strLog + "\n";
        File log = new File(LOG_AFILIACION + todayLog + ".txt");

        writeFile(log, strLog.getBytes(), true);
    }

    static void writeFinger(String strLog){

        String todayTime = new SimpleDateFormat("'|'dd-MM-yy'|'HH:mm:ss'|'").format(new Date());
        String todayLog = new SimpleDateFormat("dd-MM-yy").format(new Date());

        strLog = todayTime + strLog + "\n";
        File log = new File(LOG_AFILIACION + todayLog + ".txt");

        writeFile(log, strLog.getBytes(), true);
    }

    private static String bin2hex(byte[] data) {
        return String.format("%0" + (data.length*2) + "x", new BigInteger(1, data));
    }

    static boolean writeFile(File file, byte[] content, boolean append){
        try {
            FileOutputStream fos = new FileOutputStream(file, append);
            fos.write(content);
            fos.flush();
            fos.close();

            return true;
        } catch (Exception e) {
            return false;
        }
    }



    private static byte[] getHash(String password) {
        MessageDigest digest=null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        digest.reset();
        return digest.digest(password.getBytes());
    }

    static String readFile(String filePath) {
        String s;
        StringBuilder fileContent = new StringBuilder();
        try {
            File myFile = new File(filePath);
            FileInputStream fIn = new FileInputStream(myFile);
            BufferedReader myReader = new BufferedReader(
                    new InputStreamReader(fIn));

            while ((s = myReader.readLine()) != null) {
                fileContent.append(s).append("\n");
            }
            myReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileContent.toString();
    }
    public static Bundle getBundleValuesXML(String xmlMen){

        Log.e("Estado", "String xmlMen:" +xmlMen); //respuestaws
        Bundle values = new Bundle();

        try {
            DocumentBuilder newDocumentBuilder =
                    DocumentBuilderFactory.newInstance().newDocumentBuilder();

            Document parse = newDocumentBuilder.parse(
                    new ByteArrayInputStream(xmlMen.getBytes()));

            //LA ETIQUETA OutParam CONTIENE LA INFORMACIÓN QUE REGRESA EL SERVICIO
            NodeList node = parse.getElementsByTagName(WS_TAG_BASE);
            Node elem = node.item(0);
            Node child;
            //String name;

            if(elem != null && elem.hasChildNodes()){
                if(elem.getFirstChild().getNodeName().equals(WS_TAG_ESTADO)) {
                    Node n = parse.getElementsByTagName(WS_TAG_ESTADO).item(0);
                    values.putString(WS_TAG_HTTP_ESTADO, n.getFirstChild().getTextContent());
                    values.putString(WS_TAG_MSG, n.getFirstChild().getNextSibling().getTextContent());

                    for (child = elem.getFirstChild().getNextSibling(); child != null; child = child.getNextSibling()) {
                        values.putString(child.getNodeName(), child.getTextContent());
                    }
                }else
                    for( child = elem.getFirstChild(); child != null; child = child.getNextSibling() )
                        values.putString(child.getNodeName(), child.getTextContent());
            }

            return values;

        }catch(IOException e){
            e.getStackTrace();
            writeLog("ERROR: " + e.getMessage());
            return null;
        } catch(ParserConfigurationException e){
            e.getStackTrace();
            writeLog( "ERROR: " + e.getMessage());
            return null;
        } catch(SAXException e){
            e.getStackTrace();
            writeLog("ERROR: " + e.getMessage());
            return null;
        }
    }
}
